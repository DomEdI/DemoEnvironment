#!/bin/bash

set -e
  
cmd="$@"

>&2 echo "Laravel - check"
while true ; do
    if [ -f /var/www/public/index.php ]; then
        >&2 echo "Laravel is up"
        break
    else
        >&2 echo "Laravel is unavailable"
        sleep 1
    fi
done

exec $cmd