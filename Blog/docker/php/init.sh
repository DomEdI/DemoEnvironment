#!/bin/bash

set -e
cmd="$@"

if [ -f /var/www/public/index.php ]; then
    >&2 echo "-- Not first container startup --"
    chown -R www-data:www-data /var/www
else
    >&2 echo "-- First container startup --"
    cd /var/www
    rm -rf {,.[!.],..?}*
    composer create-project laravel/laravel . --prefer-dist
    chown -R www-data:www-data /var/www
fi

exec $cmd